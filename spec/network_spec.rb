# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Network do
  let(:test1_filename) { File.join(__dir__, 'test1.txt') }
  let(:test2_filename) { File.join(__dir__, 'test2.txt') }

  it 'correctly calculates test1.txt' do
    expect(Network::Core.new(test1_filename).process). to eq('0 2 2 2')
  end

  it 'correctly calculates test2.txt' do
    expect(Network::Core.new(test2_filename).process). to eq('2 3 3 2')
  end
end
