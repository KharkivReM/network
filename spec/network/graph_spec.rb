# frozen_string_literal: true

require 'spec_helper'
require 'pry'

RSpec.describe Network::Graph do
  let(:graph) { described_class.new([]) }
  let(:node1) { Network::GraphNode.new(1) }
  let(:node2) { Network::GraphNode.new(2) }
  let(:node3) { Network::GraphNode.new(3) }

  context 'indirect friends' do
    it 'properly finds one indirect friend' do
      node1.add_child(node2)
      node2.add_child(node3)
      [node1, node2, node3].each { |item| graph.add_node(item) }
      expect(graph.indirect_friends(node1)).to eq([node3])
    end

    it 'no indirect friend if the friend does not have any connections' do
      node1.add_child(node2)
      [node1, node2, node3].each { |item| graph.add_node(item) }
      expect(graph.indirect_friends(node1)).to be_empty
    end

    it 'does not add direct friend to the indirect list' do
      node1.add_child(node2)
      node1.add_child(node3)
      node2.add_child(node3)
      [node1, node2, node3].each { |item| graph.add_node(item) }
      expect(graph.indirect_friends(node1)).to be_empty
    end
  end
end
