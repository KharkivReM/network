# frozen_string_literal: true

require 'spec_helper'
require 'pry'

RSpec.describe Network::Core do
  let(:core_instance) { described_class.new(filename) }
  subject { core_instance.process }

  context 'valid file' do
    let(:filename) { File.join(__dir__, 'valid_file.txt') }
    before { subject }

    it 'parses people_quantity' do
      expect(core_instance.people_quantity).to eq(4)
    end

    it 'parses connections_quantity' do
      expect(core_instance.connections_quantity).to eq(3)
    end

    it 'adds connected people to graph' do
      expect(core_instance.graph.nodes.count).to eq(3)
      expect(core_instance.graph.find_node(2)).to be
      expect(core_instance.graph.find_node(3)).to be
      expect(core_instance.graph.find_node(4)).to be
    end

    it 'add connections to graph' do
      expect(core_instance.graph.find_node(2).children
             .map(&:value).sort).to eq([3, 4])
      expect(core_instance.graph.find_node(3).children
             .map(&:value).sort).to eq([2, 4])
      expect(core_instance.graph.find_node(4).children
             .map(&:value).sort).to eq([2, 3])
    end
  end

  context 'invalid summary file' do
    let(:filename) { File.join(__dir__, 'invalid_summary_file.txt') }

    it 'does not have proper number of connections' do
      expect do
        subject
      end.to raise_error(Network::InvalidFile,
                         'connections defined do not match first line')
    end
  end

  context 'invalid connections file' do
    context 'loop connection' do
      let(:filename) { File.join(__dir__, 'loop_connection_file.txt') }
      it 'raise exception' do
        expect do
          subject
        end.to raise_error(Network::InvalidFile,
                           'node cannot be connected to itself')
      end
    end

    context 'duplicate connection' do
      let(:filename) { File.join(__dir__, 'duplicate_connection_file.txt') }
      it 'raise exception' do
        expect do
          subject
        end.to raise_error(Network::InvalidFile,
                           'duplicate connection detected')
      end
    end
  end
end
