# frozen_string_literal: true

module Network
  class Graph
    attr_reader :nodes

    def initialize(nodes)
      @nodes = nodes
    end

    def add_node(node)
      @nodes << node
    end

    def find_node(value)
      @nodes.select { |node| node.value == value }.first
    end

    def indirect_friends(node)
      result = []
      node.mark_visited
      # mark all children nodes as visited
      node.children.each(&:mark_visited)
      node.children.each do |child|
        child.children.each do |element|
          unless element.visited
            element.mark_visited
            result << element
          end
        end
      end
      # cleanup visited flags
      nodes.each(&:mark_not_visited)
      result
    end
  end
end
