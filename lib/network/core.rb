# frozen_string_literal: true

module Network
  InvalidFile = Class.new(RuntimeError)
  SUMMARY_INCONSISTENT = 'connections defined do not match first line'
  LOOP_DETECTED = 'node cannot be connected to itself'
  DUPLICATE_DETECTED = 'duplicate connection detected'

  class Core
    attr_accessor :filename
    attr_reader :people_quantity, :connections_quantity, :graph

    def initialize(filename)
      @filename = filename
    end

    def process
      @graph = Network::Graph.new([])
      parse_file
      prepare_brief_output
    end

    private def parse_file
      line_number = 0
      data = File.read(filename)
      data.gsub!(/\r\n?/, "\n")
      data.each_line do |line|
        line_number.zero? ? parse_summary(line) : record_connections(line)
        line_number += 1
      end
      validate_data!(line_number)
    end

    private def validate_data!(lines_quantity)
      connection_check = connections_quantity != lines_quantity - 1
      raise InvalidFile, SUMMARY_INCONSISTENT if connection_check
    end

    private def prepare_brief_output
      result = []
      (1..people_quantity).to_a.each do |person_number|
        if (node = graph.find_node(person_number))
          result << (node.children.size + graph.indirect_friends(node).size)
        else
          result << 0
        end
      end
      result.join(' ')
    end

    # rubocop:disable Metrics/AbcSize
    private def prepare_full_output
      (1..people_quantity).to_a.each do |person_number|
        node = graph.find_node(person_number)
        STDERR.puts "Person #{person_number}:  Total - 0" && next unless node
        indirect_friends = graph.indirect_friends(node)
        STDERR.puts "Person #{person_number}: \
                    Total - #{node.children.size + indirect_friends.size} \
                    Direct_friends: #{node.children.map(&:value)}; \
                    Indirect_friends = #{indirect_friends.map(&:value)}"
      end
    end
    # rubocop:enable Metrics/AbcSize

    private def parse_summary(line)
      @people_quantity, @connections_quantity = line.split(' ').map(&:to_i)
    end

    private def record_connections(line)
      left, right = line.split(' ').map(&:to_i)
      left_node = find_or_add_node(left)
      right_node = find_or_add_node(right)
      validate_connection(left_node, right_node)
      left_node.add_child(right_node)
      right_node.add_child(left_node)
    end

    private def find_or_add_node(value)
      node = graph.find_node(value)
      return node if node
      node = GraphNode.new(value)
      graph.add_node(node)
      node
    end

    private def validate_connection(node1, node2)
      raise InvalidFile, LOOP_DETECTED if node1 == node2
      raise InvalidFile, DUPLICATE_DETECTED if node1.children.include?(node2)
    end
  end
end
