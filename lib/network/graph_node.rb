# frozen_string_literal: true

module Network
  class GraphNode
    attr_reader :value, :children, :visited

    def initialize(value, children = [])
      @value = value
      @children = children
      @visited = false
    end

    def add_child(node)
      @children << node
    end

    def mark_visited
      @visited = true
    end

    def mark_not_visited
      @visited = false
    end
  end
end
